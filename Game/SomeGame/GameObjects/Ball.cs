using System.Drawing;

namespace SomeGame.GameObjects
{
	internal class Ball : MovingSceneObject
	{
		public Ball(double posX, double posY, double velocityX, double velocityY)
			: base(posX, posY, velocityX, velocityY)
		{
		}

		public override void Draw(Graphics g)
		{
			g.FillEllipse(Brushes.Yellow,
				(int) (PositionX - 5), (int) (PositionY - 5),
				10, 10);
		}
	}
}