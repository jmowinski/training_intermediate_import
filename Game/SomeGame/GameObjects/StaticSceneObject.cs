using SomeGame.Scenes;

namespace SomeGame.GameObjects
{
	internal abstract class StaticSceneObject : SceneObject
	{
		protected StaticSceneObject(double posX, double posY)
			: base(posX, posY)
		{
		}

		public override void Update(double deltaTime)
		{
		}
	}
}