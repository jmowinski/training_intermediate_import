using System.Drawing;

namespace SomeGame.Infrastructure
{
	internal interface IScene
	{
		void Initialize();
		void Draw(Graphics g);
		void Update(double deltaTime);
	}
}