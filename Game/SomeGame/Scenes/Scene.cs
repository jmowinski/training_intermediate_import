using System;
using GameFoundation;
using SomeGame.GameObjects;

namespace SomeGame.Scenes
{
	internal class Scene : SceneBase
	{
		public Scene(int width, int height, IKeyboardStatus keyboardStatus)
			: base(width, height, keyboardStatus)
		{
		}

		public override void Initialize()
		{
			var rand = new Random();
			for (int i = 0; i < 100; ++i)
			{
				SceneObjects.Add(
					new Ball(rand.Next(20, 550), rand.Next(20, 550), 100, 100)
					);
			}
		}
	}
}