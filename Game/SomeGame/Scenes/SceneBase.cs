using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GameFoundation;
using SomeGame.Infrastructure;

namespace SomeGame.Scenes
{
	internal abstract class SceneBase : IScene
	{
		protected readonly int Height;
		protected readonly IKeyboardStatus KeyboardStatus;
		protected readonly int Width;

		protected List<ISceneObject> SceneObjects = new List<ISceneObject>();

		public SceneBase(int width, int height, IKeyboardStatus keyboardStatus)
		{
			Width = width;
			Height = height;
			KeyboardStatus = keyboardStatus;
		}

		public abstract void Initialize();

		public virtual void Draw(Graphics g)
		{
			foreach (ISceneObject obj in SceneObjects)
			{
				obj.Draw(g);
			}
		}

		public virtual void Update(double deltaTime)
		{
			foreach (ISceneObject obj in SceneObjects)
			{
				obj.Update(deltaTime);
			}

			foreach (IVelocityChangeable obj in SceneObjects.OfType<IVelocityChangeable>())
			{
				obj.Update(deltaTime);

				double vx = 0;
				double vy = 0;
				if (obj.PositionX <= 0 || obj.PositionX >= Width)
					vx = obj.VelocityX;
				if (obj.PositionY <= 0 || obj.PositionY >= Height)
					vy = obj.VelocityY;
				obj.ChangeVelocity(-2*vx, -2*vy);
			}
		}
	}
}