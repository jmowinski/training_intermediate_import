using System.Drawing;
using SomeGame.Infrastructure;

namespace SomeGame.Scenes
{
	internal abstract class SceneObject : ISceneObject
	{
		protected SceneObject(double posX, double posY)
		{
			PositionX = posX;
			PositionY = posY;
		}

		public double PositionX { get; protected set; }
		public double PositionY { get; protected set; }


		public abstract void Draw(Graphics g);
		public abstract void Update(double deltaTime);
	}
}