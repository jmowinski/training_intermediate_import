﻿using System.Drawing;
using System.Windows.Forms;
using GameFoundation;
using SomeGame.Infrastructure;
using SomeGame.Scenes;

namespace SomeGame
{
	internal class Game : GameBase
	{
		private IScene _scene;

		public Game()
			: base(800, 600)
		{
			_scene = new Scene(Width, Height, KeyboardStatus);
			_scene.Initialize();
		}

		public override string Name
		{
			get { return "Piłeczka"; }
		}

		public override void PerformNextIteration(Graphics g, double deltaTime)
		{
			g.Clear(Color.Blue);

			_scene.Update(deltaTime);
			_scene.Draw(g);

			if (KeyboardStatus.IsKeyPressed(Keys.D1))
			{
				_scene = new Scene(Width, Height, KeyboardStatus);
				_scene.Initialize();
			}
			else if (KeyboardStatus.IsKeyPressed(Keys.D2))
			{
				_scene = new OtherScene(Width, Height, KeyboardStatus);
				_scene.Initialize();
			}
			else if (KeyboardStatus.IsKeyPressed(Keys.D3))
			{
				_scene = new BallScene(Width, Height, KeyboardStatus);
				_scene.Initialize();
			}
		}
	}
}