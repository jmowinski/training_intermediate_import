﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GameFoundation
{
	public abstract class DoubleBufferedControl : Control
	{
		private BufferedGraphics _buffer;
		private BufferedGraphicsContext _bufferContext;
		private bool _dirty;

		public DoubleBufferedControl()
		{
			_bufferContext = new BufferedGraphicsContext();
			SizeGraphicsBuffer();
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, false);
			SetStyle(ControlStyles.DoubleBuffer, false);
		}

		public bool Dirty
		{
			get { return _dirty; }
			set
			{
				if (_dirty == false)
				{
					_dirty = true;
					Refresh();
				}
			}
		}

		private void SizeGraphicsBuffer()
		{
			if (_buffer != null)
			{
				_buffer.Dispose();
				_buffer = null;
			}

			if (_bufferContext == null)
				return;

			if (DisplayRectangle.Width <= 0)
				return;

			if (DisplayRectangle.Height <= 0)
				return;

			using (Graphics graphics = CreateGraphics())
			{
				_buffer = _bufferContext.Allocate(graphics, DisplayRectangle);
				_buffer.Graphics.PageUnit = GraphicsUnit.Pixel;
			}

			Dirty = true;
		}

		protected override void OnSizeChanged(EventArgs e)
		{
			SizeGraphicsBuffer();
			base.OnSizeChanged(e);
		}

		[DllImport("user32", CharSet = CharSet.Auto, EntryPoint = "ValidateRect")]
		public static extern Boolean ValidateRectangle(IntPtr handle, IntPtr nullPointer);

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			/* Do Nothing */
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (_buffer == null)
			{
				e.Graphics.PageUnit = GraphicsUnit.Pixel;
				Draw(e.Graphics);
				return;
			}

			if (_dirty)
			{
				_dirty = false;
				Invalidate(true);
				Draw(_buffer.Graphics);
				ValidateRectangle(Handle, IntPtr.Zero);
			}

			_buffer.Render(e.Graphics);
		}

		private void Draw(Graphics graphics)
		{
			if (ClientRectangle.Width <= 0)
				return;

			if (ClientRectangle.Height <= 0)
				return;

			DoDraw(graphics);
		}

		protected abstract void DoDraw(Graphics graphics);

		//idisposable
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_buffer != null)
				{
					_buffer.Dispose();
					_buffer = null;
				}

				if (_bufferContext != null)
				{
					_bufferContext.Dispose();
					_bufferContext = null;
				}
			}

			base.Dispose(disposing);
		}
	}
}