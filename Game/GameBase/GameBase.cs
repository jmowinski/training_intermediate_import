using System.Drawing;
using System.Windows.Forms;

namespace GameFoundation
{
	public abstract class GameBase : IGame
	{
		protected GameBase(int width, int height)
		{
			Width = width;
			Height = height;
			KeyboardStatus = new KeyboardDealer();
		}

		public virtual bool UsePerformanceOptimization
		{
			get { return false; }
		}

		public abstract string Name { get; }
		public int Width { get; private set; }
		public int Height { get; private set; }
		public IKeyboardStatus KeyboardStatus { get; private set; }

		public abstract void PerformNextIteration(Graphics g, double deltaTime);

		public void Initialize(IKeyboardStatus keyboardStatus)
		{
			KeyboardStatus = keyboardStatus;
		}

		protected bool IsKeyPressed(Keys key)
		{
			if (KeyboardStatus == null)
				return false;
			return KeyboardStatus.IsKeyPressed(key);
		}
	}
}