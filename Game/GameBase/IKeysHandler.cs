using System.Windows.Forms;

namespace GameFoundation
{
	public interface IKeysHandler
	{
		void KeyDown(Keys key);
		void KeyUp(Keys key);
	}
}