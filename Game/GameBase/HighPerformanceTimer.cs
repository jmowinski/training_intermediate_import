using System.ComponentModel;
using System.Runtime.InteropServices;

namespace GameFoundation
{
	internal class HighPerformanceTimer
	{
		private readonly long _freq;
		private long _startTime, _stopTime;

		public HighPerformanceTimer()
		{
			_startTime = 0;
			_stopTime = 0;

			if (QueryPerformanceFrequency(out _freq) == false)
			{
				// high-performance counter not supported
				throw new Win32Exception();
			}
		}

		public double Duration
		{
			get { return (_stopTime - _startTime)/(double) _freq; }
		}

		[DllImport("Kernel32.dll")]
		private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

		[DllImport("Kernel32.dll")]
		private static extern bool QueryPerformanceFrequency(out long lpFrequency);

		public void Start()
		{
			//for waiting threads
			//Thread.Sleep(0);
			QueryPerformanceCounter(out _startTime);
		}

		public void Stop()
		{
			QueryPerformanceCounter(out _stopTime);
		}

		public void Reset()
		{
			_startTime = 0;
			_stopTime = 0;
			QueryPerformanceCounter(out _startTime);
		}
	}
}