using System.Collections.Generic;
using System.Windows.Forms;

namespace GameFoundation
{
	public class KeyboardDealer : IKeysHandler, IKeyboardStatus
	{
		private readonly HashSet<Keys> _set = new HashSet<Keys>();

		public bool IsKeyPressed(Keys key)
		{
			return _set.Contains(key);
		}

		public void KeyDown(Keys key)
		{
			if (!_set.Contains(key))
				_set.Add(key);
		}

		public void KeyUp(Keys key)
		{
			if (_set.Contains(key))
				_set.Remove(key);
		}
	}
}